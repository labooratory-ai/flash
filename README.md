<div align="center">
    <img src="/exemplaryImages/overview.png">
</div>
<br>

# **FLASH**: **F**ramework for **LA**rge-**S**cale **H**istomorphometry

This repository represents a python framework to train, evaluate and apply segmentation networks for renal histological analysis. In particular, we trained an [nnUnet](https://github.com/MIC-DKFZ/nnUNet) for kidney tissue segmentation followed by training another [U-net-like](https://arxiv.org/pdf/1505.04597.pdf) CNN for the segmentation of several renal structures including ![#ff0000](https://via.placeholder.com/15/ff0000/000000?text=+) tubulus, ![#00ff00](https://via.placeholder.com/15/00ff00/000000?text=+) glomerulus, ![#0000ff](https://via.placeholder.com/15/0000ff/000000?text=+) glomerular tuft, ![#00ffff](https://via.placeholder.com/15/00ffff/000000?text=+) non-tissue background (including veins, renal pelvis), ![#ff00ff](https://via.placeholder.com/15/ff00ff/000000?text=+) artery, and ![#ffff00](https://via.placeholder.com/15/ffff00/000000?text=+) arterial lumen from PAS-stained histopathology data. In our experiments, we utilized human tissue data sampled from different cohorts including inhouse biopsies (AC_B) and nephrectomies (AC_N), the *Human BioMolecular Atlas Program* cohort (HuBMAP), the *Kidney Precision Medicine Project* cohort (KPMP), and the *Validation of the Oxford classification of IgA Nephropathy* cohort (VALIGA).

# Installation
1. Clone this repo using [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git):<br>
```
git clone https://git-ce.rwth-aachen.de/labooratory-ai/flash.git/
```
2. Install [miniconda](https://docs.conda.io/en/latest/miniconda.html) and use conda to create a suitable python environment as prepared in *environment.yml* that lists all library dependencies:<br>
```
conda env create -f ./environment.yml
```
3. Activate installed python environment:
```
source activate python37
```
4. Install [pytorch](https://pytorch.org/) depending on your system:
```
conda install pytorch torchvision cudatoolkit=10.2 -c pytorch
```
# Training
Train a structure segmentation network, e.g. using the following command:
```
python ./FLASH/training.py -m custom -s train_val_test -e 500 -b 6 -r 0.001 -w 0.00001
```
Note:<br>
- Before, you need to specify the path to results folder (variable: *resultsPath*) in *training.py* and the path to your data set folder (variable: *image_dir_base*) in *dataset.py*
- *training.py* is parameterized as follows:
```
training.py --model --setting --epochs --batchSize --lrate --weightDecay 
```
- We trained the prior tissue segmentation network using the [nnUnet repo](https://github.com/MIC-DKFZ/nnUNet).
# Application
Use *segment_WSI.py* to apply the trained networks for tissue and histopathological renal structure segmentation to data of your choice.
```
python ./FLASH/segment_WSI.py
```
Note: Before running the script, you need to specify the path to the image folder (variable: *WSIrootFolder*), both network paths (variable: *model_FG_path* and *modelpath*), and the results folder (variable: *resultsPath*).<br>
In particular, the script will recursively walk through all WSIs in a specified folder hierarchy and first apply the tissue segmentation CNN for segmentation. For this, the tissue is resampled to the expected pixel spacing by the CNN. The structure segmentation network is then applied to the detected tissue regions. After post-processing, the final prediction results are then saved using several (overlay) images as well as stored in numpy arrays for further feature analysis.
# Feature computation
Use *compute_features.py* to extract different morphological features from the predicted segmentations.
```
python ./FLASH/compute_features.py
```
Note: The script outputs a feature table for all structures in a *.csv* file. Each row contains features from a different instance and can thus be used to identify the instances.<br>
We applied this whole pipeline to multiple cohorts including AC_B, AC_N, HuBMAP, KPMP, and VALIGA, extracting about 40M features in total. We share those in the file *NGM_DataRepository.zip*. 
<br>
<br>
Besides, you can also apply the trained network to our provided exemplary image patches contained in the folder *exemplaryData*. These patches show various pathologies and are listed below including our ground-truth annotation:
<br>
| Cohort | Annotation |
|:--:|:--:|
| AC_B | Annotation |
| <img src="/exemplaryImages/UKA_Biopsies.png" width="400">| <br><br><img src="/exemplaryImages/UKA_Biopsies_Annotation.png" width="324"> |
| AC_N | Annotation |
| <img src="/exemplaryImages/UKA_Nephrectomy.png" width="400">| <br><br><img src="/exemplaryImages/UKA_Nephrectomy_Annotation.png" width="324"> |
| HuBMAP | Annotation |
| <img src="/exemplaryImages/HuBMAP.png" width="400">| <br><br><img src="/exemplaryImages/HuBMAP_Annotation.png" width="324"> |
| KPMP | Annotation |
| <img src="/exemplaryImages/KPMP.png" width="400">| <br><br><img src="/exemplaryImages/KPMP_Annotation.png" width="324"> |
<br>
<b>Further notes:</b>
<br>
- We showcase CNN segmentations of several thousand structures within the <i>exemplary_CNN_segmentations</i> folder.
<br>
<br>

# Contact
Peter Boor, MD, PhD<br>
Institute of Pathology<br>
RWTH Aachen University Hospital<br>
Pauwelsstrasse 30<br>
52074 Aachen, Germany<br>
Phone:	+49 241 80 85227<br>
Fax:		+49 241 80 82446<br>
E-mail: 	pboor@ukaachen.de<br>
<br>

#
    /**************************************************************************
    *                                                                         *
    *   Copyright (C) 2022 by RWTH Aachen University                          *
    *   http://www.rwth-aachen.de                                             *
    *                                                                         *
    *   License:                                                              *
    *                                                                         *
    *   This software is dual-licensed under:                                 *
    *   • Commercial license (please contact: pboor@ukaachen.de)              *
    *   • AGPL (GNU Affero General Public License) open source license        *
    *                                                                         *
    ***************************************************************************/                                                                

